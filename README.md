# docker-xrootd

An xrootd (https://github.com/xrootd/xrootd.git) Docker container definition file.

## Building

The Dockerfile supports the following arguments:

 - VERSION: Must correspond to a tag name in the git repo.

```
docker build -t xrootd -f Dockerfile .
```

## Running

This container expects the user to provide:

 - An /etc/xrootd directory which contains the xrootd config files.
 - Any other files or directories referenced in the config files should be (bind) mounted onto the container.  This includes the main data directory if need be, but also for instance, if you reference /etc/grid-security/certificates or any .pem files, these should be provided.

The xrootd process will run inside the container as `xrootd:xrootd`, which have `uid=1000`, `gid=1000`. This could be relevant for setting the appropriate permissions on files/directories residing on the host that will be bind-mounted.

One way of running via docker, assuming you already have config files and certificates required by the xrootd config, and all the directories that are bind-mounted have been chowned to uid 1000 (the xrootd user inside the container):
```
docker run --rm --name xrootd -v ./grid-security:/etc/grid-security -v ./xrootd-config:/etc/xrootd
 -v /data/test:/data -p 1096:1096 -u xrootd xrootd -c /etc/xrootd/xrootd-docker.cfg
```

## Caveats and troubleshooting

### xrootd fails during initialisation

During initialisation, xrootd segfaults (SIGSEGV) with an output that resembles the following:
```
Config maximum number of connections restricted to 1073741816
230724 12:21:09 453 Xrd_Config: sendfile enabled.
230724 12:21:09 453 Xrd_LinkCtl: Allocating 64 link objects at a time
230724 12:21:13 453 Xrd_Poll: Starting poller 0
230724 12:21:13 458 XrdPoll: Unable to poll for events; invalid argument
230724 12:21:13 453 Xrd_Poll: Starting poller 1
Aborted (core dumped)
```

Short explanation: Lower the maximum amount of file descriptors via your container environment or via ulimit -n. Or alternatively, set the limit explicitly in your xrootd config file via the `xrd.maxfd` directive:
```
xrd.maxfd strict 256k
```

Long explanation:
What's happening here is that the `epoll_wait` call is returning EINVAL, making xrootd call `abort()`. The reason for this is that xrootd is using a maximum number of file descriptors that is [too high for epoll](https://github.com/torvalds/linux/blob/6eaae198076080886b9e7d57f4ae06fa782f90ef/fs/eventpoll.c#L2299). In the example above, `1073741816` is much greater than `INT_MAX / sizeof(struct epoll_event)`, therefore making `epoll_wait` fail.

This limit is read by xrootd to be the `RLIMIT_NOFILE` limit by default. This limit just not set to a sane value by xrootd defaults, [unless it detects that the limit is infinity](https://github.com/xrootd/xrootd/blob/8f0a3ebf6b14c65c57667d07c0c624f3d4b215a8/src/Xrd/XrdConfig.cc#L1192). Therefore the solution is to force the limit with the `xrd.maxfd` directive or to set a much lower system limit before starting xrootd.

## License

See LICENSE
